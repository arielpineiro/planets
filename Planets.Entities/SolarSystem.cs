﻿using Planets.Entities.Objects;
using System.Collections.Generic;
using System.Linq;

namespace Planets.Entities
{
    public class SolarSystem
    {
        /// <summary>
        ///     Gets or sets the sun.
        /// </summary>
        /// <value>
        ///     The sun.
        /// </value>
        public Star Sun { get; set; }

        /// <summary>
        ///     The planets
        /// </summary>
        public List<Planet> Planets { get; set; }

        /// <summary>
        ///     Gets the ferengi.
        /// </summary>
        /// <returns></returns>
        public Planet Ferengi => Planets
            .Single(planet => planet.Name.Equals(nameof(Objects.Planets.Ferengi)));

        /// <summary>
        ///     Gets the betazoid.
        /// </summary>
        /// <returns></returns>
        public Planet Betazoid => Planets
            .Single(planet => planet.Name.Equals(nameof(Objects.Planets.Betazoid)));

        /// <summary>
        ///     Gets the vulcan.
        /// </summary>
        /// <returns></returns>
        public Planet Vulcan => Planets
            .Single(planet => planet.Name.Equals(nameof(Objects.Planets.Vulcan)));
    }
}