﻿namespace Planets.Entities.Weather
{
    public class WeatherForecastSummary
    {
        /// <summary>
        ///     Gets or sets the rain days.
        /// </summary>
        /// <value>
        ///     The rain days.
        /// </value>
        public int RainDays { get; set; }

        /// <summary>
        ///     Gets or sets the optimal days.
        /// </summary>
        /// <value>
        ///     The optimal days.
        /// </value>
        public int OptimalDays { get; set; }

        /// <summary>
        ///     Gets or sets the drought days.
        /// </summary>
        /// <value>
        ///     The drought days.
        /// </value>
        public int DroughtDays { get; set; }

        /// <summary>
        ///     Gets or sets the day of maximum instability.
        /// </summary>
        /// <value>
        ///     The day of maximum instability.
        /// </value>
        public int DayOfMaximumInstability { get; set; }

        /// <summary>
        ///     Gets or sets the perimeter.
        /// </summary>
        /// <value>
        ///     The perimeter.
        /// </value>
        public double Perimeter { get; set; }

        /// <summary>
        ///     Increments the day.
        /// </summary>
        /// <param name="day">The day.</param>
        public void IncrementDay(Day day)
        {
            day.Sum(this);
        }
    }
}