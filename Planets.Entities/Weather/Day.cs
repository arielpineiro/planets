﻿namespace Planets.Entities.Weather
{
    public abstract class Day
    {
        /// <summary>
        ///     Gets or sets the number.
        /// </summary>
        /// <value>
        ///     The number.
        /// </value>
        public int Number { get; set; }

        /// <summary>
        ///     Gets or sets the perimeter.
        /// </summary>
        /// <value>
        ///     The perimeter.
        /// </value>
        public double Perimeter { protected get; set; }

        /// <summary>
        ///     Sums the specified weather forecast summary.
        /// </summary>
        /// <param name="weatherForecastSummary">The weather forecast summary.</param>
        public abstract void Sum(WeatherForecastSummary weatherForecastSummary);
    }
}