﻿using Newtonsoft.Json;

namespace Planets.Entities.Weather.Conditions
{
    [JsonObject(Title = "Optimo")]
    public class OptimalDay : Day
    {
        /// <inheritdoc />
        /// <summary>
        ///     Sums the specified weather forecast summary.
        /// </summary>
        /// <param name="weatherForecastSummary">The weather forecast summary.</param>
        public override void Sum(WeatherForecastSummary weatherForecastSummary)
        {
            weatherForecastSummary.OptimalDays++;
        }

        /// <summary>
        ///     Returns a <see cref="string" /> that represents this instance.
        /// </summary>
        /// <returns>
        ///     A <see cref="string" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return "Optimo";
        }
    }
}