﻿using Newtonsoft.Json;

namespace Planets.Entities.Weather.Conditions
{
    [JsonObject(Title = "Desconocido")]
    public class UnknownDay : Day
    {
        /// <inheritdoc />
        /// <summary>
        ///     Sums the specified weather forecast summary.
        /// </summary>
        /// <param name="weatherForecastSummary">The weather forecast summary.</param>
        public override void Sum(WeatherForecastSummary weatherForecastSummary)
        {
            //Do not anything.
        }

        /// <summary>
        ///     Returns a <see cref="string" /> that represents this instance.
        /// </summary>
        /// <returns>
        ///     A <see cref="string" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return "Desconocido";
        }
    }
}