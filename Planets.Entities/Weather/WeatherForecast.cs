﻿using System.Collections.Generic;

namespace Planets.Entities.Weather
{
    public class WeatherForecast
    {
        /// <summary>
        ///     Gets or sets the days.
        /// </summary>
        /// <value>
        ///     The days.
        /// </value>
        public List<Day> Days { get; set; }
    }
}