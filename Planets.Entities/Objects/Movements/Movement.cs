﻿namespace Planets.Entities.Objects.Movements
{
    /// <summary>
    ///     Based on Strategy Pattern.
    /// </summary>
    public abstract class Movement
    {
        /// <summary>
        ///     Moves the specified planet.
        /// </summary>
        /// <param name="planet">The planet.</param>
        /// <param name="theta">The theta.</param>
        /// <param name="interval">The interval.</param>
        public abstract void Move(Planet planet, double theta, int interval);
    }
}