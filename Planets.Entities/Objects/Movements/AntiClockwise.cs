﻿using Planets.Mathematics.Geometry;

namespace Planets.Entities.Objects.Movements
{
    public class AntiClockwise : Movement
    {
        /// <inheritdoc />
        /// <summary>
        ///     Moves the specified planet.
        /// </summary>
        /// <param name="planet">The planet.</param>
        /// <param name="theta">The theta.</param>
        /// <param name="interval">The interval.</param>
        public override void Move(Planet planet, double theta, int interval)
        {
            Point point = Circle.AntiClockwisePoint(planet.Radius, theta * interval);
            planet.Coordinates.X = point.X;
            planet.Coordinates.Y = point.Y;
        }
    }
}