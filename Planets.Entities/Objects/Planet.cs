﻿using Planets.Entities.Objects.Movements;
using System;

namespace Planets.Entities.Objects
{
    public class Planet : AstronomicalObject
    {
        /// <summary>
        ///     Gets or sets the movement.
        /// </summary>
        /// <value>
        ///     The movement.
        /// </value>
        public Movement Movement { get; set; }

        /// <summary>
        ///     Moves the specified interval.
        /// </summary>
        /// <param name="interval">The interval.</param>
        public void Move(int interval)
        {
            Movement
                .Move(this, Speed * Math.PI / 180, interval);
        }
    }
}