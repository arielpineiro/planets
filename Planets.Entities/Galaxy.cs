﻿namespace Planets.Entities
{
    public class Galaxy
    {
        /// <summary>
        ///     Gets or sets the solar system.
        /// </summary>
        /// <value>
        ///     The solar system.
        /// </value>
        public SolarSystem SolarSystem { get; set; }
    }
}