﻿using Newtonsoft.Json;

namespace Planets.Dto.Weather
{
    public class DaysSummaryDto : BaseDto
    {
        /// <summary>
        ///     Uno de los días del modelo.
        /// </summary>
        /// <value>
        ///     The day.
        /// </value>
        [JsonProperty("dia")]
        public int Day { get; set; }

        /// <summary>
        ///     Rain (LLuvia)
        ///     Optimal (Condiciones optimas de presion y temperatura)
        ///     Drought (Periodo de sequia)
        ///     Unknown (Indeterminado)
        /// </summary>
        /// <value>
        ///     The weather.
        /// </value>
        [JsonProperty("clima")]
        public string Weather { get; set; }
    }
}