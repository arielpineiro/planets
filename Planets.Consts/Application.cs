﻿using static System.Configuration.ConfigurationManager;

namespace Planets.Consts
{
    public static class Application
    {
        public static class Static
        {
            public static class Javascript
            {
                /// <summary>
                ///     The library
                /// </summary>
                public const string Lib = @"/wwwroot/dist/js/lib.min.js";

                /// <summary>
                ///     The application
                /// </summary>
                public const string App = @"/wwwroot/dist/js/planets.min.js";
            }

            public static class Stylesheet
            {
                /// <summary>
                ///     The library
                /// </summary>
                public const string Lib = @"/wwwroot/dist/css/lib.min.css";

                /// <summary>
                ///     The application
                /// </summary>
                public const string App = @"/wwwroot/dist/css/planets.min.css";
            }
        }

        public static class Math
        {
            /// <summary>
            ///     The tolerance
            /// </summary>
            public const double Tolerance = .00001;

            /// <summary>
            ///     The decimals
            /// </summary>
            public const int Decimals = 1;
        }

        public static class Cache
        {
            /// <summary>
            ///     The memcached connection string
            /// </summary>
            public static readonly string MemcachedConnectionString = AppSettings["MemcachedConnectionString"];
        }

        public static class NoSql
        {
            /// <summary>
            ///     The redis connection string
            /// </summary>
            public static readonly string RedisConnectionString = AppSettings["RedisConnectionString"];

            public static class Keys
            {
                /// <summary>
                ///     The forecast
                /// </summary>
                public const string Forecast = nameof(Forecast);

                /// <summary>
                ///     The galaxy
                /// </summary>
                public const string Galaxy = nameof(Galaxy);

                /// <summary>
                ///     Volatile Key for specific Day.
                /// </summary>
                public static readonly string Volatile = "day:{0}";
            }
        }
    }
}