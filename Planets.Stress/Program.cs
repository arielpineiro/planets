﻿using RestSharp;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static System.Console;

namespace Planets.Stress
{
    public static class Program
    {
        /// <summary>
        ///     The minimum value
        /// </summary>
        private const int MinValue = 1;

        /// <summary>
        ///     The maximum value
        /// </summary>
        private const int MaxValue = 3650;

        /// <summary>
        ///     The base URL
        /// </summary>
        private const string BaseUrl = @"http://planets.apphb.com";

        /// <summary>
        ///     Defines the entry point of the application.
        /// </summary>
        private static void Main()
        {
            List<string> resources = Enumerable.Range(MinValue, MaxValue).Select(id => $"/clima?dia={id}").ToList();
            RestClient client = new RestClient(BaseUrl);

            int paging = 50;
            int i = 0;

            while (true)
            {
                IEnumerable<string> bulk = resources.Skip(i).Take(paging);
                List<Task> batch = new List<Task>();
                WriteLine($"Bulk >");
                foreach (string resource in bulk)
                {
                    batch.Add(Task.Run(() =>
                    {
                        IRestRequest request = new RestRequest(resource);
                        IRestResponse response = client.Execute(request);
                        WriteLine($"\t> {BaseUrl}{resource} ... {response.StatusCode}");
                    }));
                }
                Task.WaitAll(batch.ToArray());
                i = i + paging;
                if (i > resources.Count)
                    i = 0;
            }
        }
    }
}