# Leame #

Este documento representa una rápida introducción para poder utilizar esta herramienta.

### ¿Para que sirve Planets? ###

* Resumen

Planets contiene las utilidades necesarias para poder reconstruir una predicción meteorología hasta 10 años para los planetas del Sistema Solar (Vulcano, Betasoide y Ferengi).

Lo cual, para los habitantes de estos planetas resulta muy útil para poder estimar la inversión en sus cosechas.

### ¿Cómo utilizo la herramienta? ###

* Requerimientos
** Un entorno capaz de correr aplicaciones .NET Framework 4.6

* Configuración
** No requiere

* Dependencias 
** .NET Framework 4.6 / OS Windows (OSX/Linux si está instalado el .NET Framework).

* ¿Que opciones dispone el Job?
Planets.Jobs --help

### ![Dependencies Graph.png](https://bitbucket.org/repo/MMAxg6/images/3867962835-Dependencies%20Graph.png)