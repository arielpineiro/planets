﻿using Planets.Storage.Contracts;
using ServiceStack.Caching;
using System;

namespace Planets.Storage.ServiceStack
{
    public class MemcachedProvider : ICacheProvider
    {
        /// <summary>
        ///     The client
        /// </summary>
        private readonly ICacheClient _client;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MemcachedProvider" /> class.
        /// </summary>
        /// <param name="client">The client.</param>
        public MemcachedProvider(ICacheClient client)
        {
            _client = client;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Gets the specified key.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public T Get<T>(string key)
        {
            return _client.Get<T>(key);
        }

        /// <inheritdoc />
        /// <summary>
        ///     Adds the specified key.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public void Add<T>(string key, T value)
        {
            _client.Set(key, value);
        }

        /// <inheritdoc />
        /// <summary>
        ///     Adds the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public void Add(string key, string value)
        {
            _client.Set(key, value);
        }

        /// <inheritdoc />
        /// <summary>
        ///     Adds the specified key.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <param name="timeToLive">The time to live.</param>
        public void Add<T>(string key, T value, TimeSpan timeToLive)
        {
            _client.Set(key, value, timeToLive);
        }
    }
}