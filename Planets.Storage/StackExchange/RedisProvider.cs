﻿using Planets.Storage.Contracts;
using StackExchange.Redis.Extensions.Core.Abstractions;
using System;

namespace Planets.Storage.StackExchange
{
    public class RedisProvider : INoSqlProvider
    {
        /// <summary>
        ///     The client
        /// </summary>
        private readonly IRedisCacheClient _client;

        /// <summary>
        ///     Initializes a new instance of the <see cref="RedisProvider" /> class.
        /// </summary>
        /// <param name="client">The client.</param>
        public RedisProvider(IRedisCacheClient client)
        {
            _client = client;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Adds the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public void Add(string key, string value)
        {
            _client.GetDbFromConfiguration().AddAsync(key, value);
        }

        /// <inheritdoc />
        /// <summary>
        ///     Adds the specified key.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public void Add<T>(string key, T value)
        {
            bool result = _client.GetDbFromConfiguration().AddAsync(key, value).Result;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Adds the specified key.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <param name="timeToLive">The time to live.</param>
        public void Add<T>(string key, T value, TimeSpan timeToLive)
        {
            bool result = _client.GetDbFromConfiguration().AddAsync(key, value, timeToLive).Result;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Gets the specified key.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public T Get<T>(string key)
        {
            return _client.GetDbFromConfiguration().GetAsync<T>(key).Result;
        }
    }
}