﻿using static System.Math;

namespace Planets.Mathematics.Geometry
{
    public static class Perimeter
    {
        /// <summary>
        ///     Triangles the specified x1.
        /// </summary>
        /// <param name="x1">The x1.</param>
        /// <param name="y1">The y1.</param>
        /// <param name="x2">The x2.</param>
        /// <param name="y2">The y2.</param>
        /// <param name="x3">The x3.</param>
        /// <param name="y3">The y3.</param>
        /// <returns></returns>
        public static double Triangle(double x1, double y1, double x2, double y2, double x3, double y3)
        {
            return Sqrt(Pow(x1 - x2, 2) + Pow(y2 - y1, 2))
                   + Sqrt(Pow(x2 - x3, 2) + Pow(y3 - y2, 2))
                   + Sqrt(Pow(x3 - x1, 2) + Pow(y1 - y3, 2));
        }
    }
}