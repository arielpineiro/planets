﻿namespace Planets.Mathematics.Geometry
{
    public static class Triangle
    {
        /// <summary>
        ///     Points the inside.
        /// </summary>
        /// <param name="x1">The x1.</param>
        /// <param name="y1">The y1.</param>
        /// <param name="x2">The x2.</param>
        /// <param name="y2">The y2.</param>
        /// <param name="x3">The x3.</param>
        /// <param name="y3">The y3.</param>
        /// <param name="p1">The p1.</param>
        /// <param name="p2">The p2.</param>
        /// <returns></returns>
        public static bool PointInside(double x1, double y1, double x2, double y2, double x3, double y3, double p1,
            double p2)
        {
            var sign1 = Sign(p1, p2, x1, y1, x2, y2) < 0;
            var sign2 = Sign(p1, p2, x2, y2, x3, y3) < 0;
            var sign3 = Sign(p1, p2, x3, y3, x1, y1) < 0;

            return sign1 == sign2 && sign2 == sign3;
        }

        /// <summary>
        ///     Signs the specified x1.
        /// </summary>
        /// <param name="x1">The x1.</param>
        /// <param name="y1">The y1.</param>
        /// <param name="x2">The x2.</param>
        /// <param name="y2">The y2.</param>
        /// <param name="x3">The x3.</param>
        /// <param name="y3">The y3.</param>
        /// <returns></returns>
        private static double Sign(double x1, double y1, double x2, double y2, double x3, double y3)
        {
            return (x1 - x3) * (y2 - y3) - (x2 - x3) * (y1 - y3);
        }
    }
}