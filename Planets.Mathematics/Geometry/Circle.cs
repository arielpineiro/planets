﻿using static Planets.Consts.Application.Math;
using static System.Math;

namespace Planets.Mathematics.Geometry
{
    public static class Circle
    {
        /// <summary>
        ///     Clockwises the point.
        /// </summary>
        /// <param name="radius">The radius.</param>
        /// <param name="theta">The theta.</param>
        /// <returns></returns>
        public static Point ClockwisePoint(double radius, double theta)
        {
            Point point = new Point
            {
                X = Round(radius * Sin(theta), Decimals),
                Y = Round(radius * Cos(theta), Decimals)
            };

            return point;
        }

        /// <summary>
        ///     Antis the clockwise point.
        /// </summary>
        /// <param name="radius">The radius.</param>
        /// <param name="theta">The theta.</param>
        /// <returns></returns>
        public static Point AntiClockwisePoint(double radius, double theta)
        {
            Point point = new Point
            {
                X = Round(radius * Cos(theta + PI / 2), Decimals),
                Y = Round(radius * Sin(theta + PI / 2), Decimals)
            };

            return point;
        }
    }
}