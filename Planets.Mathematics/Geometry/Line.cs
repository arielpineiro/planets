﻿using static Planets.Consts.Application.Math;
using static System.Math;

namespace Planets.Mathematics.Geometry
{
    public static class Line
    {
        /// <summary>
        ///     Equations for two points.
        /// </summary>
        /// <param name="x1">The x1.</param>
        /// <param name="y1">The y1.</param>
        /// <param name="x2">The x2.</param>
        /// <param name="y2">The y2.</param>
        /// <param name="p1">The p1.</param>
        /// <param name="p2">The p2.</param>
        /// <returns></returns>
        public static bool EquationForTwoPoints(double x1, double y1, double x2, double y2, double p1, double p2)
        {
            return Abs(Round(p1 - x1 / (x2 - x1), Decimals) - Round(p2 - y1 / (y2 - y1), Decimals)) < Tolerance;
        }
    }
}