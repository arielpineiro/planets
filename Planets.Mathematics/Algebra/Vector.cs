﻿using static Planets.Consts.Application.Math;
using static System.Math;

namespace Planets.Mathematics.Algebra
{
    public static class Vector
    {
        /// <summary>
        ///     Ares the parallel.
        /// </summary>
        /// <param name="x1">The x1.</param>
        /// <param name="y1">The y1.</param>
        /// <param name="x2">The x2.</param>
        /// <param name="y2">The y2.</param>
        /// <param name="x3">The x3.</param>
        /// <param name="y3">The y3.</param>
        /// <returns></returns>
        public static bool AreParallel(double x1, double y1, double x2, double y2, double x3, double y3)
        {
            return Abs(Round((x2 - x1) / (x3 - x2), Decimals) - Round((y2 - y1) / (y3 - y2), Decimals)) < Tolerance;
        }

        /// <summary>
        ///     Theys the are on axes.
        /// </summary>
        /// <param name="x1">The x1.</param>
        /// <param name="y1">The y1.</param>
        /// <param name="x2">The x2.</param>
        /// <param name="y2">The y2.</param>
        /// <param name="x3">The x3.</param>
        /// <param name="y3">The y3.</param>
        /// <returns></returns>
        public static bool TheyAreOnAxes(double x1, double y1, double x2, double y2, double x3, double y3)
        {
            if (Abs(x1) < Tolerance && Abs(x2) < Tolerance && Abs(x3) < Tolerance)
                return true;

            return Abs(y1) < Tolerance && Abs(y2) < Tolerance && Abs(y3) < Tolerance;
        }
    }
}