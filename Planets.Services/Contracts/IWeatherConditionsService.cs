﻿using Planets.Entities.Weather;

namespace Planets.Services.Contracts
{
    public interface IWeatherConditionsService
    {
        /// <summary>
        ///     Gets the wheater forecast.
        /// </summary>
        /// <param name="days">The days.</param>
        /// <returns></returns>
        WeatherForecast GetWheaterForecast(int days);

        /// <summary>
        ///     Gets the wheater forecast summary.
        /// </summary>
        /// <param name="days">The days.</param>
        /// <returns></returns>
        WeatherForecastSummary GetWheaterForecastSummary(int days);
    }
}