﻿using Planets.Entities;
using Planets.Entities.Weather;
using Planets.Entities.Weather.Conditions;
using Planets.Mathematics.Algebra;
using static Planets.Mathematics.Algebra.Vector;
using static Planets.Mathematics.Geometry.Line;

namespace Planets.Services.WeatherConditions.Rules
{
    public class PlanetsAreAlignedWithTheSunRule : BaseRule
    {
        /// <inheritdoc />
        /// <summary>
        ///     Gets the condition.
        /// </summary>
        /// <param name="solarSystem">The solar system.</param>
        /// <returns></returns>
        public override Day Request(SolarSystem solarSystem)
        {
            return ThePlanetsAreAlignedWithTheSun(solarSystem) ? new DroughtDay() : Next.Request(solarSystem);
        }

        /// <summary>
        ///     Thes the planets are aligned with the sun.
        /// </summary>
        /// <param name="solarSystem"></param>
        /// <returns></returns>
        private bool ThePlanetsAreAlignedWithTheSun(SolarSystem solarSystem)
        {
            if (TheyAreOnAxes(solarSystem))
                return true;
            if (ThePlanetAreAlignedBetweenThem(solarSystem))
                if (TheyPassThroughTheSun(solarSystem))
                    return true;
            return false;
        }

        /// <summary>
        ///     Theys the are on axes.
        /// </summary>
        /// <param name="solarSystem"></param>
        /// <returns></returns>
        private bool TheyAreOnAxes(SolarSystem solarSystem)
        {
            var x1 = solarSystem.Ferengi.Coordinates.X;
            var y1 = solarSystem.Ferengi.Coordinates.Y;
            var x2 = solarSystem.Betazoid.Coordinates.X;
            var y2 = solarSystem.Betazoid.Coordinates.Y;
            var x3 = solarSystem.Vulcan.Coordinates.X;
            var y3 = solarSystem.Vulcan.Coordinates.Y;

            return Vector.TheyAreOnAxes(x1, y1, x2, y2, x3, y3);
        }

        /// <summary>
        ///     Thes the planet are aligned between them.
        /// </summary>
        /// <param name="solarSystem"></param>
        /// <returns></returns>
        private bool ThePlanetAreAlignedBetweenThem(SolarSystem solarSystem)
        {
            var x1 = solarSystem.Ferengi.Coordinates.X;
            var y1 = solarSystem.Ferengi.Coordinates.Y;
            var x2 = solarSystem.Betazoid.Coordinates.X;
            var y2 = solarSystem.Betazoid.Coordinates.Y;
            var x3 = solarSystem.Vulcan.Coordinates.X;
            var y3 = solarSystem.Vulcan.Coordinates.Y;

            return AreParallel(x1, y1, x2, y2, x3, y3);
        }

        /// <summary>
        ///     Theys the pass through the sun.
        /// </summary>
        /// <param name="solarSystem"></param>
        /// <returns></returns>
        private bool TheyPassThroughTheSun(SolarSystem solarSystem)
        {
            var x1 = solarSystem.Ferengi.Coordinates.X;
            var y1 = solarSystem.Ferengi.Coordinates.Y;
            var x2 = solarSystem.Betazoid.Coordinates.X;
            var y2 = solarSystem.Betazoid.Coordinates.Y;

            return EquationForTwoPoints(x1, y1, x2, y2, 0, 0);
        }
    }
}