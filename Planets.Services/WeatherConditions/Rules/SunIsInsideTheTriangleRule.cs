﻿using Planets.Entities;
using Planets.Entities.Weather;
using Planets.Entities.Weather.Conditions;
using static Planets.Mathematics.Geometry.Triangle;

namespace Planets.Services.WeatherConditions.Rules
{
    public class SunIsInsideTheTriangleRule : BaseRule
    {
        /// <inheritdoc />
        /// <summary>
        ///     Gets the condition.
        /// </summary>
        /// <param name="solarSystem">The solar system.</param>
        /// <returns></returns>
        public override Day Request(SolarSystem solarSystem)
        {
            return TheSunIsInsideTheTriangle(solarSystem) ? new RainDay() : Next.Request(solarSystem);
        }

        /// <summary>
        ///     Thes the sun is inside the triangle.
        /// </summary>
        /// <param name="solarSystem">The solar system.</param>
        /// <returns></returns>
        private bool TheSunIsInsideTheTriangle(SolarSystem solarSystem)
        {
            var x1 = solarSystem.Ferengi.Coordinates.X;
            var y1 = solarSystem.Ferengi.Coordinates.Y;
            var x2 = solarSystem.Betazoid.Coordinates.X;
            var y2 = solarSystem.Betazoid.Coordinates.Y;
            var x3 = solarSystem.Vulcan.Coordinates.X;
            var y3 = solarSystem.Vulcan.Coordinates.Y;
            var p1 = solarSystem.Sun.Coordinates.X;
            var p2 = solarSystem.Sun.Coordinates.Y;

            return PointInside(x1, y1, x2, y2, x3, y3, p1, p2);
        }
    }
}