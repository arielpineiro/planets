﻿using Planets.Entities;
using Planets.Entities.Weather;
using Planets.Entities.Weather.Conditions;

namespace Planets.Services.WeatherConditions.Rules
{
    public class UnknownRule : BaseRule
    {
        /// <inheritdoc />
        /// <summary>
        ///     Gets the condition.
        /// </summary>
        /// <param name="solarSystem">The solar system.</param>
        /// <returns></returns>
        public override Day Request(SolarSystem solarSystem)
        {
            return new UnknownDay();
        }
    }
}