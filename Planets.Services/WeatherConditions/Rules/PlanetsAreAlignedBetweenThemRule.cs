﻿using Planets.Entities;
using Planets.Entities.Weather;
using Planets.Entities.Weather.Conditions;
using static Planets.Mathematics.Algebra.Vector;

namespace Planets.Services.WeatherConditions.Rules
{
    public class PlanetsAreAlignedBetweenThemRule : BaseRule
    {
        /// <inheritdoc />
        /// <summary>
        ///     Gets the condition.
        /// </summary>
        /// <param name="solarSystem">The solar system.</param>
        /// <returns></returns>
        public override Day Request(SolarSystem solarSystem)
        {
            return ThePlanetAreAlignedBetweenThem(solarSystem) ? new OptimalDay() : Next.Request(solarSystem);
        }

        /// <summary>
        ///     Thes the planet are aligned between them.
        /// </summary>
        /// <param name="solarSystem"></param>
        /// <returns></returns>
        private bool ThePlanetAreAlignedBetweenThem(SolarSystem solarSystem)
        {
            var x1 = solarSystem.Ferengi.Coordinates.X;
            var y1 = solarSystem.Ferengi.Coordinates.Y;
            var x2 = solarSystem.Betazoid.Coordinates.X;
            var y2 = solarSystem.Betazoid.Coordinates.Y;
            var x3 = solarSystem.Vulcan.Coordinates.X;
            var y3 = solarSystem.Vulcan.Coordinates.Y;

            return AreParallel(x1, y1, x2, y2, x3, y3);
        }
    }
}