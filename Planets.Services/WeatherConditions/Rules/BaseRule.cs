﻿using Planets.Entities;
using Planets.Entities.Weather;

namespace Planets.Services.WeatherConditions.Rules
{
    public abstract class BaseRule
    {
        /// <summary>
        ///     The next
        /// </summary>
        protected BaseRule Next;

        /// <summary>
        ///     Sets the next.
        /// </summary>
        /// <param name="rule">The rule.</param>
        public BaseRule SetNext(BaseRule rule)
        {
            Next = rule;
            return this;
        }

        /// <summary>
        ///     Gets the condition.
        /// </summary>
        /// <param name="solarSystem">The solar system.</param>
        /// <returns></returns>
        public abstract Day Request(SolarSystem solarSystem);
    }
}