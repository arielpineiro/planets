﻿using Planets.Entities;
using Planets.Entities.Objects;
using Planets.Entities.Weather;
using Planets.Mathematics.Geometry;
using Planets.Repositories.Universe;
using Planets.Services.Contracts;
using Planets.Services.WeatherConditions.Rules;
using System.Collections.Generic;

namespace Planets.Services.WeatherConditions
{
    public class WeatherConditionsService : IWeatherConditionsService
    {
        private readonly IGalaxyRepository _galaxyRepository;

        /// <summary>
        ///     The rule
        /// </summary>
        private readonly BaseRule _rule = new PlanetsAreAlignedWithTheSunRule();

        /// <summary>
        ///     Initializes a new instance of the <see cref="WeatherConditionsService" /> class.
        /// </summary>
        public WeatherConditionsService(IGalaxyRepository galaxyRepository)
        {
            _galaxyRepository = galaxyRepository;

            _rule
                .SetNext(new PlanetsAreAlignedBetweenThemRule()
                    .SetNext(new SunIsInsideTheTriangleRule()
                        .SetNext(new UnknownRule())));
        }

        /// <inheritdoc />
        /// <summary>
        ///     Gets the wheater forecast summary.
        /// </summary>
        /// <param name="days">The days.</param>
        /// <returns></returns>
        public WeatherForecastSummary GetWheaterForecastSummary(int days)
        {
            WeatherForecastSummary summary = new WeatherForecastSummary();

            SolarSystem solarSystem = _galaxyRepository.GetSolarSystem();

            for (var number = 0; number <= days; number++)
            {
                Day day = GetDayFromNumberOfTheDay(solarSystem, number);
                summary.IncrementDay(day);
            }

            return summary;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Gets the wheater forecast.
        /// </summary>
        /// <param name="days">The days.</param>
        /// <returns></returns>
        public WeatherForecast GetWheaterForecast(int days)
        {
            WeatherForecast weatherForecast = new WeatherForecast { Days = new List<Day>() };

            SolarSystem solarSystem = _galaxyRepository.GetSolarSystem();

            for (var i = 0; i <= days; i++)
            {
                Day day = GetDayFromNumberOfTheDay(solarSystem, i);
                weatherForecast.Days.Add(day);
            }

            return weatherForecast;
        }

        /// <summary>
        ///     Gets the day conditions.
        /// </summary>
        /// <param name="solarSystem">The solar system.</param>
        /// <returns></returns>
        private Day GetSpecificDay(SolarSystem solarSystem)
        {
            return _rule.Request(solarSystem);
        }

        /// <summary>
        ///     Gets the day from number of the day.
        /// </summary>
        /// <param name="solarSystem">The solar system.</param>
        /// <param name="i">The i.</param>
        /// <returns></returns>
        private Day GetDayFromNumberOfTheDay(SolarSystem solarSystem, int i)
        {
            StartSimulation(solarSystem, i);

            Day day = GetSpecificDay(solarSystem);
            day.Perimeter = DegreeOfInstability(solarSystem);
            day.Number = i;

            return day;
        }

        /// <summary>
        ///     Starts the simulation.
        /// </summary>
        /// <param name="solarSystem">The solar system.</param>
        /// <param name="day">The day.</param>
        private void StartSimulation(SolarSystem solarSystem, int day)
        {
            foreach (Planet planet in solarSystem.Planets)
                planet.Move(day);
        }

        /// <summary>
        ///     Degrees the of instability.
        /// </summary>
        /// <param name="solarSystem"></param>
        /// <returns></returns>
        private double DegreeOfInstability(SolarSystem solarSystem)
        {
            var x1 = solarSystem.Ferengi.Coordinates.X;
            var y1 = solarSystem.Ferengi.Coordinates.Y;
            var x2 = solarSystem.Betazoid.Coordinates.X;
            var y2 = solarSystem.Betazoid.Coordinates.Y;
            var x3 = solarSystem.Vulcan.Coordinates.X;
            var y3 = solarSystem.Vulcan.Coordinates.Y;

            return Perimeter.Triangle(x1, y1, x2, y2, x3, y3);
        }
    }
}