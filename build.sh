#!/bin/bash
( cd Planets.Api ; npm install )
( cd Planets.Api ; gulp min )
nuget restore
msbuild /m /nr:false /t:Clean,Rebuild /p:Configuration=Debug /p:Platform="Any CPU" /verbosity:minimal /p:CreateHardLinksForCopyLocalIfPossible=true
echo