﻿var gulp = require("gulp");
var concat = require("gulp-concat");
var cssmin = require("gulp-cssmin");
var uglify = require("gulp-uglify");
var sourcemaps = require("gulp-sourcemaps");
var del = require("del");

var folders = {
    webroot: "./wwwroot/",
    nodemodules: "./node_modules/"
};

var paths = {
    js: folders.webroot + "js/**/*.js",
    minJs: folders.webroot + "js/**/*.min.js",
    css: folders.webroot + "css/**/*.css",
    minCss: folders.webroot + "css/**/*.min.css",
    concatJsDest: folders.webroot + "dist/js/planets.min.js",
    concatCssDest: folders.webroot + "dist/css/planets.min.css",
    jsDist: folders.webroot + "dist/js",
    cssDist: folders.webroot + "dist/css",
    lib: {
        "js": [
            folders.nodemodules + "jquery/dist/jquery.min.js",
            folders.nodemodules + "bootstrap/dist/js/bootstrap.min.js"
        ],
        "css": [
            folders.nodemodules + "bootstrap/dist/css/bootstrap.min.css"
        ],
        "fonts": folders.nodemodules + "bootstrap/dist/fonts/**/*",
        "concatJsDest": folders.webroot + "dist/js/lib.min.js",
        "concatCssDest": folders.webroot + "dist/css/lib.min.css",
        "fontsDest": folders.webroot + "fonts",
        "mapsDest": folders.webroot + "dist/css/"
    }
};

gulp.task('clean:js',
    function () {
        return del(paths.jsDist);
    });

gulp.task("clean:css",
    function () {
        return del(paths.cssDist);
    });

gulp.task("clean", gulp.series('clean:js', 'clean:css'));

gulp.task("min:js",
    function () {
        return gulp.src([paths.js, "!" + paths.minJs],
            {
                base: ".",
                sourcemaps: true
            })
            .pipe(gulp.dest("."))
            .pipe(uglify())
            .pipe(concat(paths.concatJsDest))
            .pipe(sourcemaps.write("./"))
            .pipe(gulp.dest("."));
    });

gulp.task("min:css",
    function () {
        return gulp.src([paths.css, "!" + paths.minCss], { sourcemaps: true })
            .pipe(concat(paths.concatCssDest))
            .pipe(cssmin())
            .pipe(sourcemaps.write("./"))
            .pipe(gulp.dest("."));
    });

gulp.task("lib:js",
    function () {
        return gulp.src(paths.lib.js, { sourcemaps: true })
            .pipe(uglify())
            .pipe(concat(paths.lib.concatJsDest))
            .pipe(sourcemaps.write("./"))
            .pipe(gulp.dest("."));
    });

gulp.task("lib:css",
    function () {
        return gulp.src(paths.lib.css, { sourcemaps: true })
            .pipe(concat(paths.lib.concatCssDest))
            .pipe(sourcemaps.write("./"))
            .pipe(gulp.dest("."));
    });

gulp.task("lib:fonts",
    function () {
        return gulp.src(paths.lib.fonts)
            .pipe(gulp.dest(paths.lib.fontsDest));
    });

gulp.task('min:lib', gulp.parallel('lib:js', 'lib:css', 'lib:fonts'));

gulp.task('min', gulp.series('clean', gulp.parallel('min:js', 'min:css', 'min:lib')));

var build = gulp.series('clean', 'min');

gulp.task('default', build);