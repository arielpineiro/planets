module.exports = {
    "extends": "eslint:recommended",
    "rules": {
        "no-console": 0,
        "angular/log": 0
    },
    "env": {
        "browser": true,
        "node": true
    }
};