﻿using Planets.Dto;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static NewRelic.Api.Agent.NewRelic;

namespace Planets.Api.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// </summary>
    /// <seealso cref="T:System.Web.Http.ApiController" />
    public abstract class BaseApiController : ApiController
    {
        /// <summary>
        ///     Gets the specified resource identifier.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="resourceId">The resource identifier.</param>
        /// <param name="predicate">The predicate.</param>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        /// <exception cref="HttpResponseException"></exception>
        protected static T Get<T>(int resourceId, Func<int, T> predicate, string message) where T : BaseDto
        {
            try
            {
                T response = predicate(resourceId);

                if (response == default(T))
                {
                    HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.NotFound)
                    {
                        ReasonPhrase = "Resource not found.",
                        Content = new StringContent(message)
                    };
                    throw new HttpResponseException(httpResponseMessage);
                }

                return response;
            }
            catch (Exception e)
            {
                NoticeError(e);
                throw;
            }
        }
    }
}