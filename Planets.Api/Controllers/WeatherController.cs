﻿using Planets.Dto.Weather;
using Planets.Repositories.Universe;
using System.Net.Http;
using System.Web.Http;
using WebApi.OutputCache.V2;

namespace Planets.Api.Controllers
{
    /// <inheritdoc />
    /// <summary>
    ///     Obtiene las predicciones del clima.
    /// </summary>
    public class WeatherController : BaseApiController
    {
        private readonly IWeatherRepository _weatherRepository;

        /// <summary>
        ///     Initializes a new instance of the <see cref="WeatherController" /> class.
        /// </summary>
        /// <param name="weatherRepository">The provider.</param>
        public WeatherController(IWeatherRepository weatherRepository)
        {
            _weatherRepository = weatherRepository;
        }

        /// <summary>
        ///     Obtiene el resultado de un dìa en particular.
        ///     Rain (LLuvia)
        ///     Optimal (Condiciones optimas de presion y temperatura)
        ///     Drought (Periodo de sequia)
        ///     Unknown (Indeterminado)
        /// </summary>
        /// <param name="dia">The dia.</param>
        /// <returns></returns>
        /// <exception cref="HttpResponseException">Por ejemplo si el recurso no existe. </exception>
        /// <exception cref="HttpResponseMessage"></exception>
        /// <exception cref="StringContent"></exception>
        [Route("clima/{dia?}")]
        [HttpGet]
        [CacheOutput(ClientTimeSpan = 100, ServerTimeSpan = 100, AnonymousOnly = true)]
        public DaysSummaryDto GetDaysSummaryDto(int dia)
        {
            return Get(dia, resourceId => _weatherRepository.GetSummaryDto(dia),
                $"Condition for {dia} is not available. ");
        }
    }
}