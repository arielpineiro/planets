﻿using System.Web.Mvc;

namespace Planets.Api.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// </summary>
    /// <seealso cref="T:System.Web.Mvc.Controller" />
    public class HomeController : Controller
    {
        /// <summary>
        ///     Indexes this instance.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            ViewBag.Title = "Planets";

            return View();
        }
    }
}