﻿using Planets.Api.Filters;
using System.Web.Mvc;

namespace Planets.Api
{
    /// <summary>
    /// </summary>
    public static class FilterConfig
    {
        /// <summary>
        ///     Registers the global filters.
        /// </summary>
        /// <param name="filters">The filters.</param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new CompressAttribute());
        }
    }
}