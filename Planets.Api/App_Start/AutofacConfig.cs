﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using static System.Reflection.Assembly;
using static System.Web.Http.GlobalConfiguration;
using static System.Web.Mvc.DependencyResolver;

namespace Planets.Api
{
    /// <summary>
    ///     AppConfiguration.
    /// </summary>
    public static class AutofacConfig
    {
        /// <summary>
        ///     Gets or sets the container.
        /// </summary>
        /// <value>
        ///     The container.
        /// </value>
        private static IContainer Container { get; set; }

        /// <summary>
        ///     Configures the container.
        /// </summary>
        public static void ConfigureContainer()
        {
            ContainerBuilder containerBuilder = new ContainerBuilder();

            // Register dependencies in controllers
            containerBuilder.RegisterControllers(GetExecutingAssembly());
            containerBuilder.RegisterApiControllers(GetExecutingAssembly());

            // Register dependencies in filter attributes
            containerBuilder.RegisterFilterProvider();

            // Register dependencies in custom views
            containerBuilder.RegisterSource(new ViewRegistrationSource());

            Container = containerBuilder.Build();

            // Set MVC DI resolver to use our Autofac container
            SetResolver(new AutofacDependencyResolver(Container));
            Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(Container);
        }
    }
}