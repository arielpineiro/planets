﻿using Planets.Api.Jobs;
using Quartz;
using System;
using System.Threading.Tasks;
using static Quartz.Impl.StdSchedulerFactory;
using static Quartz.JobBuilder;
using static System.TimeSpan;

namespace Planets.Api
{
    /// <summary>
    ///     Simple Trigger.
    /// </summary>
    public static class JobScheduler
    {
        /// <summary>
        ///     !ONLY for Free AppHarbor License.
        /// </summary>
        public static void Start()
        {
            RunJob().GetAwaiter().GetResult();
        }

        /// <summary>
        ///     Runs the job.
        /// </summary>
        /// <returns></returns>
        private static async Task RunJob()
        {
            IScheduler scheduler = await GetDefaultScheduler();
            await scheduler.Start();

            IJobDetail job = Create<KeepAliveJob>().Build();

            ITrigger trigger = TriggerBuilder
                .Create()
                .StartAt(DateTimeOffset.Now.AddSeconds(20))
                .WithSimpleSchedule(x => x
                    .WithInterval(FromSeconds(1))
                    .RepeatForever())
                .Build();

            await scheduler.ScheduleJob(job, trigger);
        }
    }
}