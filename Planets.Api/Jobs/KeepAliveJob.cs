﻿using CircuitBreaker.Net.Exceptions;
using Quartz;
using RestSharp;
using System;
using System.Threading.Tasks;

namespace Planets.Api.Jobs
{
    /// <summary>
    ///     Only for AppHarbor.
    /// </summary>
    /// <seealso cref="Quartz.IJob" />
    /// <inheritdoc />
    public class KeepAliveJob : IJob
    {
        private static readonly RestClient Client = new RestClient("http://planets.apphb.com/");

        /// <inheritdoc />
        /// <summary>
        ///     Internal request.
        /// </summary>
        /// <param name="context"></param>
        public async Task Execute(IJobExecutionContext context)
        {
            var day = new Random().Next(1, 365 * 10);
            RestRequest request = new RestRequest($"clima/{day}", Method.GET);

            // Initialize the circuit breaker
            CircuitBreaker.Net.CircuitBreaker circuitBreaker = new CircuitBreaker.Net.CircuitBreaker(
                TaskScheduler.Default,
                3,
                TimeSpan.FromMilliseconds(100),
                TimeSpan.FromMilliseconds(10000));

            try
            {
                // perform a potentially fragile call through the circuit breaker
                await Client.ExecuteGetAsync(request);
            }
            catch (CircuitBreakerOpenException)
            {
                // the service is unavailable, failover here
            }
            catch (CircuitBreakerTimeoutException)
            {
                // handle timeouts
            }
            catch (Exception)
            {
                // handle other unexpected exceptions
            }
        }
    }
}