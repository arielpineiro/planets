﻿using Planets.Dto.Weather;
using Planets.Storage.Contracts;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Planets.Consts.Application;
using static System.String;
using static System.Threading.Tasks.Task;
using static System.TimeSpan;

namespace Planets.Repositories.Universe
{
    public class NoSqlWeatherRepository : IWeatherRepository
    {
        /// <summary>
        ///     The forecast key name
        /// </summary>
        private const string ForecastKeyName = NoSql.Keys.Forecast;

        /// <summary>
        ///     The cache provider
        /// </summary>
        private readonly ICacheProvider _cacheProvider;

        /// <summary>
        ///     The no SQL provider
        /// </summary>
        private readonly INoSqlProvider _noSqlProvider;

        /// <summary>
        ///     Initializes a new instance of the <see cref="NoSqlWeatherRepository" /> class.
        /// </summary>
        /// <param name="noSqlProvider">The no SQL provider.</param>
        /// <param name="cacheProvider">The cache provider.</param>
        public NoSqlWeatherRepository(INoSqlProvider noSqlProvider, ICacheProvider cacheProvider)
        {
            _noSqlProvider = noSqlProvider;
            _cacheProvider = cacheProvider;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Gets the summary dto.
        /// </summary>
        /// <param name="day">The day.</param>
        /// <returns></returns>
        public DaysSummaryDto GetSummaryDto(int day)
        {
            var key = Format(NoSql.Keys.Volatile, day);
            DaysSummaryDto summaryDto = _cacheProvider.Get<DaysSummaryDto>(key);

            if (summaryDto != null)
                return summaryDto;

            summaryDto = _noSqlProvider.Get<DaysSummaryDto>(key);

            if (summaryDto != null)
                return summaryDto;

            List<DaysSummaryDto> forecast = _noSqlProvider.Get<List<DaysSummaryDto>>(ForecastKeyName);

            if (forecast != null)
            {
                summaryDto = forecast.FirstOrDefault(item => item.Day == day);
                if (summaryDto != null)
                {
                    Task[] tasks =
                    {
                        Factory.StartNew(() => { _cacheProvider.Add(key, summaryDto, FromMinutes(30)); }),
                        Factory.StartNew(() => { _noSqlProvider.Add(key, summaryDto, FromHours(1)); })
                    };

                    WaitAll(tasks);
                }
            }

            return summaryDto;
        }
    }
}