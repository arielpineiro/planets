﻿using Planets.Consts;
using Planets.Entities;
using Planets.Storage.Contracts;

namespace Planets.Repositories.Universe
{
    public class NoSqlGalaxyRepository : IGalaxyRepository
    {
        /// <summary>
        ///     The galaxy Key Name for NoSQL.
        /// </summary>
        private const string GalaxyKeyName = Application.NoSql.Keys.Galaxy;

        /// <summary>
        ///     The storage provider
        /// </summary>
        private readonly INoSqlProvider _storageProvider;

        /// <summary>
        ///     Initializes a new instance of the <see cref="NoSqlGalaxyRepository" /> class.
        /// </summary>
        /// <param name="storageProvider">The storage provider.</param>
        public NoSqlGalaxyRepository(INoSqlProvider storageProvider)
        {
            _storageProvider = storageProvider;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Gets the default galaxy.
        /// </summary>
        /// <returns></returns>
        public Galaxy GetDefaultGalaxy()
        {
            return _storageProvider.Get<Galaxy>(GalaxyKeyName);
        }

        /// <inheritdoc />
        /// <summary>
        ///     Get the default Solar System
        /// </summary>
        /// <returns></returns>
        public SolarSystem GetSolarSystem()
        {
            return GetDefaultGalaxy().SolarSystem;
        }
    }
}