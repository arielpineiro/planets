﻿using Planets.Entities;

namespace Planets.Repositories.Universe
{
    public interface IGalaxyRepository
    {
        /// <summary>
        ///     Gets the default galaxy.
        /// </summary>
        /// <returns></returns>
        Galaxy GetDefaultGalaxy();

        /// <summary>
        ///     Get the default Solar System
        /// </summary>
        /// <returns></returns>
        SolarSystem GetSolarSystem();
    }
}