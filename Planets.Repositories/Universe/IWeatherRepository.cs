﻿using Planets.Dto.Weather;

namespace Planets.Repositories.Universe
{
    public interface IWeatherRepository
    {
        /// <summary>
        ///     Gets the summary dto.
        /// </summary>
        /// <param name="day">The day.</param>
        /// <returns></returns>
        DaysSummaryDto GetSummaryDto(int day);
    }
}