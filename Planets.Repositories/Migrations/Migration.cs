﻿namespace Planets.Repositories.Migrations
{
    public abstract class Migration
    {
        /// <summary>
        ///     Do migration into repository.
        /// </summary>
        public abstract void Up();

        /// <summary>
        ///     Undo migration into repository.
        /// </summary>
        public abstract void Down();
    }
}