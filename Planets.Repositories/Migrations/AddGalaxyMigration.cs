﻿using Planets.Entities;
using Planets.Entities.Objects;
using Planets.Entities.Objects.Movements;
using Planets.Entities.Objects.Planets;
using Planets.Entities.Objects.Stars;
using Planets.Storage.Contracts;
using System;
using System.Collections.Generic;

namespace Planets.Repositories.Migrations
{
    public class AddGalaxyMigration : Migration
    {
        /// <summary>
        ///     The storage provider
        /// </summary>
        private readonly INoSqlProvider _storageProvider;

        /// <summary>
        ///     Initializes a new instance of the <see cref="AddGalaxyMigration" /> class.
        /// </summary>
        /// <param name="storageProvider">The storage provider.</param>
        public AddGalaxyMigration(INoSqlProvider storageProvider)
        {
            _storageProvider = storageProvider;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Ups this instance.
        /// </summary>
        public override void Up()
        {
            Galaxy galaxy = CreateGalaxy(new List<Planet>
            {
                CreateFerengi(),
                CreateBetazoid(),
                CreateVulcan()
            });

            _storageProvider
                .Add(nameof(Galaxy), galaxy);
        }

        /// <summary>
        ///     Creates the vulcan.
        /// </summary>
        /// <returns></returns>
        private static Planet CreateVulcan()
        {
            return new Vulcan
            {
                Name = nameof(Vulcan),
                Coordinates = new Coordinates { X = 0, Y = 1000 },
                Radius = 1000,
                Speed = 5,
                Movement = new AntiClockwise()
            };
        }

        /// <summary>
        ///     Creates the betazoid.
        /// </summary>
        /// <returns></returns>
        private static Planet CreateBetazoid()
        {
            return new Betazoid
            {
                Name = nameof(Betazoid),
                Coordinates = new Coordinates { X = 0, Y = 2000 },
                Radius = 2000,
                Speed = 3,
                Movement = new Clockwise()
            };
        }

        /// <summary>
        ///     Creates the ferengi.
        /// </summary>
        /// <returns></returns>
        private static Planet CreateFerengi()
        {
            return new Ferengi
            {
                Name = nameof(Ferengi),
                Coordinates = new Coordinates { X = 0, Y = 500 },
                Radius = 500,
                Speed = 1,
                Movement = new Clockwise()
            };
        }

        private static Galaxy CreateGalaxy(List<Planet> planets)
        {
            return new Galaxy
            {
                SolarSystem = new SolarSystem
                {
                    Sun = new Sun
                    {
                        Name = nameof(Sun),
                        Coordinates = new Coordinates
                        {
                            X = 0,
                            Y = 0
                        }
                    },
                    Planets = planets
                }
            };
        }

        /// <inheritdoc />
        /// <summary>
        ///     Downs this instance.
        /// </summary>
        /// <exception cref="T:System.NotImplementedException"></exception>
        public override void Down()
        {
            //TBD: storageProvider.Remove("Galaxy");
            throw new NotImplementedException();
        }
    }
}