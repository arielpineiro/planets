﻿namespace Planets.Jobs.Manager
{
    public abstract class Invoker : IJob
    {
        /// <summary>
        ///     The arguments
        /// </summary>
        protected readonly string[] Args;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Invoker" /> class.
        /// </summary>
        /// <param name="args">The arguments.</param>
        protected Invoker(string[] args)
        {
            Args = args;
        }

        /// <summary>
        ///     Executes this instance.
        /// </summary>
        public abstract void Execute();
    }
}