using NDesk.Options.Fork;
using NDesk.Options.Fork.Common;
using Planets.Jobs.Manager.Jobs;
using static System.Console;
using static System.Convert;

namespace Planets.Jobs.Manager
{
    public class NDeskInvoker : Invoker
    {
        /// <summary>
        ///     The option set
        /// </summary>
        private readonly OptionSet _optionSet = new OptionSet();

        /// <summary>
        ///     Initializes a new instance of the <see cref="NDeskInvoker" /> class.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public NDeskInvoker(string[] args) : base(args)
        {
        }

        /// <summary>
        ///     Executes this instance.
        /// </summary>
        public override void Execute()
        {
            var showHelp = false;

            _optionSet.Add("ci|create-initial", "Creates the initial conditions. ",
                years => new CreateInitialConditionsJob().Execute());
            _optionSet.Add("c|conditions=", "Show contidions for years. ",
                years => new ShowConditionsJob(ToInt32(years) * 365).Execute());
            _optionSet.Add("cm|create-model=", "Create the models into KeyValue. ",
                years => new CreateModelJob(ToInt32(years) * 365).Execute());
            _optionSet.Add("h|help", "Show this message and exit", v => showHelp = v != null);

            try
            {
                _optionSet.Parse(Args);
            }
            catch (OptionException optionException)
            {
                WriteLine(optionException.Message);
                WriteLine("Try Planets.Jobs --help' for more information.");
            }

            if (showHelp)
                ShowHelp(_optionSet);
        }

        /// <summary>
        ///     Shows the help.
        /// </summary>
        /// <param name="optionSet">The option set.</param>
        private static void ShowHelp(OptionSet optionSet)
        {
            WriteLine("Usage: Planets.Jobs");
            WriteLine();
            WriteLine("Options:");
            optionSet.WriteOptionDescriptions(Out);
        }
    }
}