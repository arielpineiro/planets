﻿using Autofac;
using Planets.Dto.Weather;
using Planets.Entities.Weather;
using Planets.Services.Contracts;
using Planets.Storage.Contracts;
using System.Collections.Generic;
using System.Linq;
using static Planets.Jobs.Program;
using static System.Console;
using static System.ConsoleColor;

namespace Planets.Jobs.Manager.Jobs
{
    public class CreateModelJob : IJob
    {
        /// <summary>
        ///     The years
        /// </summary>
        private readonly int _years;

        /// <summary>
        ///     Initializes a new instance of the <see cref="CreateModelJob" /> class.
        /// </summary>
        /// <param name="years">The years.</param>
        public CreateModelJob(int years)
        {
            _years = years;
        }

        /// <summary>
        ///     Executes this instance.
        /// </summary>
        public void Execute()
        {
            ForegroundColor = Yellow;
            WriteLine($"\n Creating database weather forecast for {_years / 365} year(s)...\n");
            using (ILifetimeScope scope = Container.BeginLifetimeScope())
            {
                IWeatherConditionsService weatherConditions = scope.Resolve<IWeatherConditionsService>();
                INoSqlProvider provider = scope.Resolve<INoSqlProvider>();

                WeatherForecast forecast = weatherConditions.GetWheaterForecast(_years);

                List<DaysSummaryDto> summaryDto = forecast.Days.Select(builderBase.Mapper.Map<DaysSummaryDto>).ToList();

                provider.Add("Forecast", summaryDto);
            }

            ForegroundColor = Green;
            WriteLine("Done. ");
            ResetColor();
        }
    }
}