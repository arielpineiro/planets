﻿using Autofac;
using Planets.Dto.Weather;
using Planets.Services.Contracts;
using static Planets.Jobs.Program;
using static System.Console;
using static System.ConsoleColor;

namespace Planets.Jobs.Manager.Jobs
{
    public class ShowConditionsJob : IJob
    {
        /// <summary>
        ///     The years
        /// </summary>
        private readonly int _years;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ShowConditionsJob" /> class.
        /// </summary>
        /// <param name="years">The years.</param>
        public ShowConditionsJob(int years)
        {
            _years = years;
        }

        /// <summary>
        ///     Executes this instance.
        /// </summary>
        public void Execute()
        {
            ForegroundColor = Yellow;
            WriteLine("\n Retrieving conditions for year(s)...\n");
            using (ILifetimeScope scope = Container.BeginLifetimeScope())
            {
                IWeatherConditionsService weatherConditions = scope.Resolve<IWeatherConditionsService>();
                WeatherForecastSummaryDto summaryDto =
                    builderBase.Mapper.Map<WeatherForecastSummaryDto>(
                        weatherConditions.GetWheaterForecastSummary(_years));

                WriteLine($"\t1) Periodos de sequia: [{summaryDto.DroughtDays}]");
                WriteLine($"\t2) Periodos de lluvia: [{summaryDto.RainDays}]");
                WriteLine($"\t\ta) Maximo dia de inestabilidad: [{summaryDto.DayOfMaximumInstability}]");
                WriteLine($"\t\tb) Perimetro: [{summaryDto.Perimeter} Km]");
                WriteLine($"\t3) Periodos de condiciones optimas de presion y temperatura: [{summaryDto.OptimalDays}]");
            }

            ResetColor();
        }
    }
}