﻿namespace Planets.Jobs.Manager
{
    public interface IJob
    {
        /// <summary>
        ///     Executes this instance.
        /// </summary>
        void Execute();
    }
}