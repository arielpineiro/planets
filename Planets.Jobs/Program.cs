﻿using Autofac;
using Planets.IoC;
using Planets.Jobs.Manager;

namespace Planets.Jobs
{
    public static class Program
    {
        public static BuilderBase builderBase = new JobsBuilder();
        public static readonly IContainer Container = builderBase.Build();

        /// <summary>
        ///     Mains the specified arguments.
        /// </summary>
        /// <param name="args">The arguments.</param>
        private static void Main(string[] args)
        {
            Invoker invoker = new NDeskInvoker(args);
            invoker.Execute();
        }
    }
}