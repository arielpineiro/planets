@echo off
cmd /c "cd /d Planets.Api && npm cache verify"
cmd /c "cd /d Planets.Api && npm install"
cmd /c "cd /d Planets.Api && gulp min"
nuget restore
msbuild /m /nr:false /t:Clean,Rebuild /p:Configuration=Debug /p:Platform="Any CPU" /verbosity:minimal /p:CreateHardLinksForCopyLocalIfPossible=true
echo.