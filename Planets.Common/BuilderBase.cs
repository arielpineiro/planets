﻿using Autofac;
using AutoMapper;
using Newtonsoft.Json;
using Planets.Dto.Weather;
using Planets.Entities.Weather;
using Planets.Repositories.Universe;
using Planets.Services.Contracts;
using Planets.Services.WeatherConditions;
using Planets.Storage.Contracts;
using Planets.Storage.ServiceStack;
using Planets.Storage.StackExchange;
using ServiceStack.Caching.Memcached;
using StackExchange.Redis.Extensions.Core;
using StackExchange.Redis.Extensions.Core.Abstractions;
using StackExchange.Redis.Extensions.Core.Configuration;
using StackExchange.Redis.Extensions.Core.Implementations;
using StackExchange.Redis.Extensions.Newtonsoft;
using System;
using static Newtonsoft.Json.NullValueHandling;
using static Newtonsoft.Json.TypeNameHandling;
using static Planets.Consts.Application.NoSql;

namespace Planets.IoC
{
    public abstract class BuilderBase : ContainerBuilder
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="BuilderBase" /> class.
        /// </summary>
        protected BuilderBase()
        {
            this.Register(serializer => new NewtonsoftSerializer(new JsonSerializerSettings
            {
                TypeNameHandling = All,
                NullValueHandling = Include
            })).As<ISerializer>().SingleInstance();

            this.Register(cacheClient =>
            {
                string connectionString = RedisConnectionString;

                RedisConfiguration redisConfiguration = new RedisConfiguration()
                {
                    AbortOnConnectFail = true,
                    Hosts = new RedisHost[]
                    {
                        new RedisHost()
                        {
                            Host = connectionString.Split(',')[0].Split(':')[0],
                            Port = Convert.ToInt32(connectionString.Split(',')[0].Split(':')[1])
                        }
                    },
                    AllowAdmin = true,
                    Password = connectionString.Split(',')[1].Split('=')[1],
                    ConnectTimeout = Convert.ToInt32(connectionString.Split(',')[2].Split('=')[1]),
                    SyncTimeout = Convert.ToInt32(connectionString.Split(',')[4].Split('=')[1]),
                    Database = 0,
                    ServerEnumerationStrategy = new ServerEnumerationStrategy()
                    {
                        Mode = ServerEnumerationStrategy.ModeOptions.All,
                        TargetRole = ServerEnumerationStrategy.TargetRoleOptions.Any,
                        UnreachableServerAction = ServerEnumerationStrategy.UnreachableServerActionOptions.Throw
                    },
                    PoolSize = 50
                };

                IRedisCacheConnectionPoolManager redisCacheConnectionPoolManager = new RedisCacheConnectionPoolManager(redisConfiguration);
                ISerializer serializer = Container.Resolve<ISerializer>();
                return new RedisCacheClient(redisCacheConnectionPoolManager, serializer, redisConfiguration);
            })
                .As<IRedisCacheClient>().SingleInstance();

            this.Register<ServiceStack.Caching.ICacheClient>(
                 cacheClient => new MemcachedClientCache(MemcachedConfigurationFactory.Build()))
                .As<ServiceStack.Caching.ICacheClient>().SingleInstance();

            this.Register(redisProvider => new RedisProvider(Container.Resolve<IRedisCacheClient>()))
                .As<INoSqlProvider>().SingleInstance();

            this.Register(
                 redisProvider => new MemcachedProvider(Container.Resolve<ServiceStack.Caching.ICacheClient>()))
                .As<ICacheProvider>().SingleInstance();

            this.Register(galaxyRepository => new NoSqlGalaxyRepository(Container.Resolve<INoSqlProvider>()))
                .As<IGalaxyRepository>().SingleInstance();

            this.Register(weatherRepository => new NoSqlWeatherRepository(Container.Resolve<INoSqlProvider>(),
                 Container.Resolve<ICacheProvider>()))
                .As<IWeatherRepository>().SingleInstance();

            this.Register(
                 weatherConditionsService => new WeatherConditionsService(Container.Resolve<IGalaxyRepository>()))
                .As<IWeatherConditionsService>().SingleInstance();

            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<WeatherForecastSummary, WeatherForecastSummaryDto>();
                cfg.CreateMap<Day, DaysSummaryDto>()
                   .ForMember(source => source.Weather, option => option.MapFrom(source => source.GetType().Name))
                   .ForMember(source => source.Day, option => option.MapFrom(source => source.Number));
            });

            this.Mapper = config.CreateMapper();
        }

        /// <summary>
        ///     Gets or sets the container.
        /// </summary>
        /// <value>
        ///     The container.
        /// </value>
        public static IContainer Container { get; set; }

        /// <summary>
        /// Gets the mapper.
        /// </summary>
        /// <value>
        /// The mapper.
        /// </value>
        public IMapper Mapper { get; }

        /// <summary>
        ///     Builds this instance.
        /// </summary>
        /// <returns></returns>
        public IContainer Build()
        {
            Container = base.Build();
            return Container;
        }
    }
}